#!/usr/bin/env bash

files=( */*.bytes )

if ! [[ -e "${files[0]}" ]]; then
  echo "${0##*/}: no files found" >&2
  exit 1
fi

set -e

printf '%s\n' "${files[@]}" >./files
git hash-object "${files[@]}" >./githashes
sha1sum "${files[@]}" >./sha1sums
