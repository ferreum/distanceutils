
from distance.bytes import Magic
from distance.classes import CollectorGroup


Classes = CollectorGroup()


add_tag = Classes.fragments.add_tag
add_tag('VirusSpiritSpawner', Magic[2], 0x3a)
add_tag('WingCorruptionZone', Magic[2], 0x53)
add_tag('SphericalGravity', Magic[2], 0x5f)
add_tag('TrackAttachment', Magic[2], 0x68)
add_tag('TurnLightOnNearCar', Magic[2], 0x70)


# vim:set sw=4 et:
