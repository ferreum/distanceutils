from construct import (
    Struct, Default, Computed, If, Bytes, this,
)

from distance.bytes import Magic, Section
from distance.construct import (
    BaseConstructFragment,
    UInt, DstString, Remainder, Float, Byte,
)
from distance.classes import CollectorGroup


Classes = CollectorGroup()


@Classes.fragments.fragment
class BiodomeFragment(BaseConstructFragment):

    base_container = Section.base(Magic[2], 0x77)
    container_versions = 0, 2, 3, 4, 23

    def get_unk_1_size(this):
        if this.version == 0:
            return 0x6f
        elif 2 <= this.version <= 3:
            return 0x77
        elif this.version == 4:
            return 0x20
        elif this.version == 23:
            return 0xe2
        assert False, f"version: {this.version}"

    _construct_ = Struct(
        'version' / Computed(this._params.sec.version),
        'unk_0' / If(this.version == 4, Bytes(0x57)),
        'background_layer_name_old' / If(this.version == 4, Default(DstString, 'Background')),
        'unk_1' / Bytes(get_unk_1_size),
        'sky_far' / If(this.version >= 3, Default(Float, 1200.0)),
        'sky_fade_distance' / Default(Float, 50.0),
        'skybox_fog_amount' / Default(Float, 0.0),
        'background_layer_enabled' / If(this.version >= 23, Default(Byte, 1)),
        'background_layer_name' / If(this.version >= 23, Default(DstString, "Background")),
        'background_scroll' / If(this.version >= 23, Default(Float, 10.0)),
        'unk_2' / If(this.version >= 23, UInt),
        'fog_enabled' / Default(Byte, 1),
        'rem' / Remainder,
    )

    del get_unk_1_size


# vim:set sw=4 et:
