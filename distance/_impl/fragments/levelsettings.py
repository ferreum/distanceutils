

from construct import (
    Struct, Sequence,
    PrefixedArray, If, IfThenElse, Computed,
    this,
)

from distance.bytes import Magic, Section
from distance.construct import (
    BaseConstructFragment,
    Int, UInt, Bytes, Byte, Float,
    DstString, Remainder,
)
from distance.classes import CollectorGroup
from distance._common import (
    ModesMapperProperty,
    MedalTimesMapperProperty,
    MedalScoresMapperProperty,
)
from distance._impl.level_content.levelsettings_base import BaseLevelSettings


Classes = CollectorGroup()


@Classes.fragments.fragment
class LevelSettingsFragment(BaseLevelSettings, BaseConstructFragment):

    base_container = Section.base(Magic[2], 0x52)
    container_versions = tuple(range(0, 28))

    is_interesting = True

    def get_unk_2_size(this):
        version = this.version
        if version <= 3:
            return 57
        elif version == 4:
            return 141
        elif version == 5:
            return 172
        elif 6 <= version < 25:
            # confirmed only for v6..v9
            return 111
        else:
            # confirmed for v25..v26
            return 218

    def get_unk_4_size(this):
        version = this.version
        if version <= 5:
            return 0
        elif 6 <= version < 25:
            # confirmed for v7..v9
            return 52
        elif 25 <= version < 27:
            # confirmed for v25..v26
            return 49
        else:
            return 52

    _construct_ = Struct(
        'version' / Computed(this._params.sec.version),
        'unk_0' / Bytes(8),
        'name' / DstString,
        'description' / If(this.version >= 25, DstString),
        'author_name' / If(this.version >= 25, DstString),
        'unk_1' / Bytes(4),
        'modes_list' / PrefixedArray(UInt, Struct(
            'mode' / UInt,
            'enabled' / Byte,
        )),
        'music_id' / If(this.version < 27, UInt),
        'music_name' / If(this.version >= 27, DstString),
        'skybox_name' / If(this.version <= 3, DstString),
        'unk_2' / Bytes(get_unk_2_size),
        # 3x confirmed for v8
        'sky_far' / If(this.version >= 6, Float),
        'sky_fade_distance' / If(this.version >= 6, Float),
        'skybox_fog_amount' / If(this.version >= 6, Float),
        # 6x confirmed for v7..v9 and v25..v26
        'background_layer_enabled' / If(this.version >= 25, Byte),
        'background_layer' / If(this.version >= 25, DstString),
        'background_scroll' / If(this.version >= 25, Float),
        'unk_3' / If(this.version >= 25, UInt),
        'fog_enabled' / If(this.version >= 6, IfThenElse(lambda this: 25 <= this.version < 27, UInt, Byte)),
        'unk_4' / Bytes(get_unk_4_size),
        'medals' / Struct(
            'time' / Float,
            'score' / Int,
        )[4],
        'abilities' / If(this.version >= 1, Sequence(Byte, Byte, Byte, Byte, Byte)),
        'difficulty' / If(this.version >= 2, UInt),
        'unk_5' / Remainder,
    )

    _add_fields_ = dict(
        modes = (),
        medal_times = None,
        medal_scores = None,
    )

    del get_unk_2_size
    del get_unk_4_size

    modes = ModesMapperProperty('modes_list')

    medal_times = MedalTimesMapperProperty('medals')

    medal_scores = MedalScoresMapperProperty('medals')


# vim:set sw=4 et:
