

from distance.bytes import Magic, Section
from distance.base import Fragment
from distance.constants import LAYER_FLAG_NAMES
from distance.printing import need_counters, print_objects
from distance.classes import CollectorGroup


Classes = CollectorGroup()


def format_layer_flags(gen):
    for flag, names in gen:
        name = names.get(flag, f"Unknown({flag})")
        if name:
            yield name


@Classes.level_content.fragment
class Layer(Fragment):

    class_tag = 'Layer'
    default_container = Section(Magic[7])

    layer_name = None
    layer_flags = (0, 0, 1)
    objects = ()
    flags_version = 1
    is_background = 0

    def __init__(self, *args, **kw):
        if 'flags_version' not in kw:
            try:
                level_version = kw.pop('level_version')
            except KeyError:
                pass
            else:
                kw['flags_version'] = None if level_version <= 2 else 1
        super().__init__(*args, **kw)

    def _read_section_data(self, dbytes, sec, *, level_version=None):
        if sec.magic != Magic[7]:
            raise ValueError(f"Invalid layer section: {sec.magic}")
        self.layer_name = sec.name

        if level_version is None:
            # Use heuristic to guess layer version.
            if sec.content_size < 4:
                # If we are too short we don't have flags and no objects.
                version = None
            else:
                # If the uint contains a known version (0 or 1) we
                # use that. Otherwise, we assume we have an old level without
                # flags.
                version = dbytes.read_uint()
                if version not in (0, 1):
                    version = None
        elif level_version <= 2:
            # layers of old levels don't have any flags
            version = None
        else:
            # New level version - read version.
            version = dbytes.read_uint()
        self.flags_version = version
        if version == 0:
            flags = dbytes.read_bytes(3)
            frozen = 1 if flags[0] == 0 else 0
            self.layer_flags = (flags[1], frozen, flags[2])
            obj_start = sec.content_start + 7
        elif version == 1:
            flags = dbytes.read_bytes(4)
            self.layer_flags = tuple(flags[:3])
            self.is_background = flags[3]
            obj_start = sec.content_start + 8
        elif version is None:
            obj_start = sec.content_start
        else:
            raise ValueError(f"Unknown flags version: {version}")
        self.objects = self.classes.level_objects.lazy_n_maybe(
            dbytes, sec.count, start_pos=obj_start)

    def _get_write_section(self, sec):
        return Section(Magic[7], name=self.layer_name)

    def _write_section_data(self, dbytes, sec):
        if sec.magic != Magic[7]:
            raise ValueError(f"Invalid layer section: {sec.magic}")
        version = self.flags_version
        if version is not None:
            flags = self.layer_flags
            dbytes.write_uint(version)
            if version == 0:
                flags_bytes = [0 if flags[1] else 1, flags[0], flags[2]]
            elif version == 1:
                flags_bytes = [flags[0], flags[1], flags[2], self.is_background]
            else:
                raise ValueError(f"Unknown flags version: {version}")
            dbytes.write_bytes(bytes(flags_bytes))
        for obj in self.objects:
            obj.write(dbytes)

    def _repr_detail(self):
        supstr = super()._repr_detail()
        return f" {self.layer_name!r}{supstr}"

    def _print_type(self, p):
        p(f"Layer: {self.layer_name!r}")

    def visit_print(self, p):
        with need_counters(p) as counters:
            yield super().visit_print(p)
            if counters:
                counters.print(p)

    def _visit_print_data(self, p):
        yield super()._visit_print_data(p)
        p(f"Layer object count: {len(self.objects)}")
        if self.layer_flags:
            flag_str = ', '.join(
                format_layer_flags(zip(self.layer_flags, LAYER_FLAG_NAMES)))
            if not flag_str:
                flag_str = "None"
            p(f"Layer flags: {flag_str}")
        p(f"Background layer: {self.is_background and 'yes' or 'no'}")
        p.counters.num_layers += 1
        p.counters.layer_objects += len(self.objects)

    def _visit_print_children(self, p):
        yield super()._visit_print_children(p)
        yield print_objects(p, self.objects)


# vim:set sw=4 et:
