

from distance.base import BaseObject
from distance.classes import CollectorGroup, DefaultClasses


Classes = CollectorGroup()


@Classes.level_content.object
@DefaultClasses.fragments.fragment_attrs('LevelSettings')
class NewLevelSettings(BaseObject):

    type = 'LevelSettings'

    def _read_section_data(self, dbytes, sec, *, level_version=None):
        # level_version doesn't matter for us here
        super()._read_section_data(dbytes, sec)


# vim:set sw=4 et:
