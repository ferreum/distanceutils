

from distance.levelobjects import LevelObject
from distance.classes import CollectorGroup, DefaultClasses
from distance.base import Transform
from distance.printing import need_counters, print_objects


Classes = CollectorGroup()


@Classes.level_objects.object
@Classes.common.object
@DefaultClasses.fragments.fragment_attrs('Group', 'CustomName')
class Group(LevelObject):

    child_classes_name = 'level_objects'
    is_object_group = True
    type = 'Group'

    default_transform = Transform.fill()

    def _visit_print_children(self, p):
        with need_counters(p) as counters:
            num = len(self.children)
            if num:
                p(f"Grouped objects: {num}")
                if 'groups' in p.flags:
                    p.counters.grouped_objects += num
                    yield print_objects(p, self.children)
            if counters:
                counters.print(p)

    def retransform(self, transform):
        old_transform = self.transform
        self.transform = transform
        for obj in self.children:
            obj.transform = old_transform.apply(*obj.transform).unapply(*transform)

    def recenter(self, center):
        self.retransform(self.transform.set(pos=center))

    def rerotate(self, rot):
        self.retransform(self.transform.set(rot=rot))

    def rescale(self, scale):
        self.retransform(self.transform.set(scale=scale))


# vim:set sw=4 et:
