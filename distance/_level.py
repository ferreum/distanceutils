"""Level file support."""


from operator import attrgetter
from collections.abc import Sequence

from .bytes import Magic, Section
from .base import Fragment
from .lazy import LazySequence
from .printing import need_counters
from .classes import CollectorGroup, DefaultClasses


Classes = CollectorGroup()


fragment_attrs = DefaultClasses.fragments.fragment_attrs


class _FilteredSequenceView(Sequence):

    """Simple but expensive and unsafe filtered read-only view for a list."""

    __slots__ = ('_source', '_pred')

    def __init__(self, source, pred):
        self._source = source
        self._pred = pred

    def __len__(self):
        return sum(1 for i in self._source if self._pred(i))

    def __getitem__(self, index):
        return list(filter(self._pred, self._source))[index]

    def __iter__(self):
        return filter(self._pred, self._source)


@Classes.level.fragment
class Level(Fragment):

    class_tag = 'Level'
    default_container = Section(Magic[9])

    _layers = ()
    _content = ()
    name = None
    version = 3

    content = property(attrgetter('_content'),
                       doc="The list of all level content.")

    @content.setter
    def content(self, value):
        self._layers = _FilteredSequenceView(value, lambda obj: obj.class_tag == 'Layer')
        self._content = value

    def _read_section_data(self, dbytes, sec):
        if sec.magic != Magic[9]:
            raise ValueError(f"Unexpected section: {sec.magic}")
        self.name = sec.name
        self.version = sec.version

        num_layers = sec.count

        self.content = self.classes.level_content.lazy_n_maybe(
            dbytes, num_layers + 1, level_version=self.version)
        self._layers = LazySequence(
            (obj for obj in self._content if obj.class_tag == 'Layer'),
            num_layers)

    layers = property(attrgetter('_layers'))

    def _get_write_section(self, sec):
        return Section(Magic[9], self.name, len(self._content) - 1, self.version)

    def _visit_write_section_data(self, dbytes, sec):
        if sec.magic != Magic[9]:
            raise ValueError(f"Unexpected section: {sec.magic}")
        for obj in self._content:
            yield obj.visit_write(dbytes)

    def add_layer(self, name, **kw):
        layer = self.classes.level_content.create(
            'Layer',
            level_version=self.version,
            layer_name=name,
            **kw
        )
        self.content += [layer]
        return layer

    @property
    def settings(self):
        try:
            return self._settings
        except AttributeError:
            for obj in self._content:
                if obj.class_tag != 'Layer':
                    s = obj
                    break
            else:
                s = None
            self._settings = s
            return s

    @settings.setter
    def settings(self, s):
        self._settings = s

    def _repr_detail(self):
        supstr = super()._repr_detail()
        if self.name:
            return f" {self.name!r}{supstr}"
        return supstr

    def visit_print(self, p):
        with need_counters(p) as counters:
            yield super().visit_print(p)
            if counters:
                counters.print(p)

    def _print_type(self, p):
        p(f"Level: {self.name!r} version {self.version}")

    def _visit_print_data(self, p):
        yield super()._visit_print_data(p)
        p(f"Level name: {self.name!r}")

    def _visit_print_children(self, p):
        if self.settings is not None:
            with p.tree_children(1):
                yield self.settings.visit_print(p)
        for layer in self.layers:
            yield layer.visit_print(p)


# vim:set sw=4 ts=8 sts=4 et:
