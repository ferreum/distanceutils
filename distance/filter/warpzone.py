"""Filter for generating a warp zone."""


from operator import attrgetter
from uuid import uuid4 as random_uuid

from distance.base import Transform
from distance.classes import DefaultClasses
from distance.constants import ForceType
from distance import Level
from .base import ObjectFilter, TeleporterTracker, create_replacement_group
from .visualize import default_boxcollider_values, default_boxcollider_values_by_type


class DestinationInfo(object):

    def __init__(self, obj, pos, tele, link_id, lines):
        self.objects = [obj]
        self.positions = [pos]
        self.tele = tele
        self.link_id = link_id
        self.lines = lines
        self.delete_object = False
        self.add_objects = []
        self.dest_destinfos = []
        self.has_hit_events = False
        self.is_checkpoint = False

    def setup_events(self):
        self.has_hit_events = True
        self.event_hit = str(random_uuid())
        self.event_reset = str(random_uuid())


def create_trigger(event_name, custom_name, transform, *, sphere=False):
    trigger = DefaultClasses.level_objects.create(
        sphere and 'EventTriggerSphere' or 'EventTriggerBox',
        transform=transform,
    )
    trigger.attach_new_fragment(
        'EventTrigger', version=2,
        event_name=event_name,
        one_shot=0,
    )
    if sphere:
        trigger.attach_new_fragment(
            'SphereCollider', version=1,
            trigger_radius=1,
            trigger_center=(0, 0, 0)
        )
    else:
        trigger.attach_new_fragment(
            'BoxCollider', version=2,
            trigger_size=(1, 1, 1),
        )
    trigger.attach_new_fragment(
        'CustomName', version=0,
        custom_name=custom_name
    )
    return trigger


common_destination_anim_settings = dict(
    motion_mode='advanced',
    do_rotate=0,
    do_scale=0,
    delay=0,
    duration=.1,
    do_loop=0,
    do_extend=1,
    curve_type='linear',
    anim_physics=0,
    always_animate=1,
    default_action='none',
    trigger_on_action='play',
    trigger_wait_for_anim_finish=1,
    trigger_off_action='stop',
    trigger_off_wait_for_anim_finish=1,
)


def setup_hit_trigger(destinfo, trigger, transform):
    trigger.attach_new_fragment(
        'Animator', version=11,
        translate_vector=(0, 2000, 0),
        translate_type='global_',
        **common_destination_anim_settings
    )
    trigger.attach_new_fragment(
        'EventListener', version=2,
        event_name=destinfo.event_hit,
    )
    trigger_group = DefaultClasses.level_objects.create(
        'Group',
        transform=transform,
        children=[trigger],
        custom_name="DestinationTrigger",
    )
    trigger_group.attach_new_fragment(
        'Animator', version=11,
        translate_vector=(0, -2000, 0),
        translate_type='local',
        **common_destination_anim_settings
    )
    trigger_group.attach_new_fragment(
        'EventListener', version=2,
        event_name=destinfo.event_reset,
    )
    destinfo.add_objects.append(trigger_group)


class DestinationMatcher(object):

    def __init__(self, filter):
        self.filter = filter

    def apply(self, obj, global_transform):
        raise NotImplementedError

    def post_process(self):
        pass


class StartZoneMatcher(DestinationMatcher):

    def disable_nitronic_startzone(obj):
        # couldn't find a fake endzone, so just change it into a regular one
        # with very high delay_before_broadcast
        obj.type = 'NitronicEndZone'
        win_logic = DefaultClasses.level_subobjects.create('WinLogic')
        win_logic.attach_new_fragment('RaceEndLogic', version=0, delay_before_broadcast=999999)
        obj.children += [win_logic]
        world_text = DefaultClasses.level_objects.create(
            'WorldText',
            transform=obj.transform.apply(pos=(0, 7/16, 200/16), scale=(1/16, 1/16, 1/16)),
            text="START"
        )
        obj.attach_new_fragment(
            'TrackSegment', version=3,
            include_in_track_calculation=False
        )
        return world_text,

    object_infos = {
        'EmpireStartZone': (Transform.fill(pos=(0, 0, 168/16), rot=(0, 1, 0, 0), scale=1/16), 'EmpireEndZoneFake'),
        'EmpireStartZoneSimple': (Transform.fill(pos=(0, 0, 1), rot=(0, 1, 0, 0), scale=1/16), 'EmpireEndZoneSimpleFake'),
        'InvisibleStartZone': (Transform.fill(), None),
        'NitronicStartZone': (Transform.fill(pos=(0, 0, 157.6/16), rot=(0, 1, 0, 0), scale=1/16), disable_nitronic_startzone),
        'NitronicStartZoneWall': (Transform.fill(pos=(0, 15, -6.5), rot=(0, 2**.5/2, 2**.5/2, 0), scale=1/4), 'NitronicEndzoneWall_FAKE'),
        'AdventureStartZone': (Transform.fill(pos=(0, 0, 32.965/16), rot=(0, 1, 0, 0), scale=1/16), 'AdventureStartZoneVisualOnly'),
    }

    def apply(self, obj, global_transform):
        if any(1 for c in obj.children if c.type == 'WinLogic'):
            self.filter.end_positions.append(global_transform.apply(*obj.transform).pos)
        try:
            exit_transform, replacement = self.object_infos[obj.type]
        except KeyError:
            return ()
        self.filter.start_positions.append(global_transform.apply(*obj.transform).apply(*exit_transform).pos)
        dest = self.filter.create_destination(obj, global_transform, transform=exit_transform)
        obj.real_transform = obj.transform
        if replacement is None:
            dest.delete_object = True
        elif callable(replacement):
            dest.add_objects.extend(replacement(obj))
        else:
            obj.type = replacement
            obj.attach_new_fragment(
                'TrackSegment', version=3,
                include_in_track_calculation=False
            )
        return dest,


class CheckpointMatcher(DestinationMatcher):

    def apply(self, obj, global_transform):
        if obj.has_any('CheckpointLogic'):
            match = obj
        else:
            try:
                match = next(c for c in obj.children if c.has_any('CheckpointLogic'))
            except StopIteration:
                return ()
        destinfo = self.filter.create_destination(obj, global_transform)
        destinfo.is_checkpoint = True
        try:
            coll = match['BoxCollider']
        except KeyError:
            pass
        else:
            default_values = default_boxcollider_values_by_type.get(obj.type, default_boxcollider_values)
            trigger_center = coll.trigger_center or default_values['trigger_center']
            trigger_size = coll.trigger_size or default_values['trigger_size']
            def create_cooldown(obj):
                cooldown = DefaultClasses.level_objects.create(
                    'CooldownTriggerNoVisual',
                    transform=obj.transform,
                )
                cooldown.attach_new_fragment(
                    'BoxCollider', version=2,
                    trigger_center=trigger_center,
                    trigger_size=trigger_size,
                )
                cooldown.attach_new_fragment(
                    'CooldownTrigger', version=1,
                    include_in_track_calculation=False,
                )
                return cooldown
            destinfo.add_objects += [create_cooldown(obj), create_cooldown(obj)]
            destinfo.setup_events()
            trigger = create_trigger(
                destinfo.event_hit,
                f"DestinationTriggerFor{obj.type}",
                Transform.fill(scale=trigger_size),
            )
            setup_hit_trigger(destinfo, trigger, obj.transform.apply(pos=trigger_center))
            coll.trigger_center = (0, 100000, 0)
            coll.trigger_size = (0, 0, 0)
        return destinfo,


class TeleporterMatcher(DestinationMatcher):

    def __init__(self, filter):
        super().__init__(filter)
        self.destinfo_by_subtele = {}
        self.destinfos = []

    def apply(self, obj, global_transform):
        try:
            match = next(c for c in obj.children
                         if c.has_any('TeleporterExit') or c.has_any('TeleporterEntrance'))
        except StopIteration:
            return ()
        pos = global_transform.apply(pos=obj.transform.pos).pos
        try:
            link_id = match['TeleporterExit'].link_id
        except KeyError:
            link_id = None
        destinfo = DestinationInfo(obj, pos, None, link_id, [obj.type])
        destinfo.__sub_teleporter = match
        try:
            checkpt = match['TeleporterExitCheckpoint']
        except KeyError:
            pass
        else:
            if checkpt.trigger_checkpoint:
                destinfo.is_checkpoint = True
                checkpt.trigger_checkpoint = 0
        has_collider = False
        if obj.type == 'TeleporterExit':
            trigger_radius = .5
            trigger_center = (0, 0, 0)
            has_collider = True
        else:
            try:
                collider = match['SphereCollider']
            except KeyError:
                pass
            else:
                trigger_radius = collider.trigger_radius
                if trigger_radius is None:
                    trigger_radius = .5
                trigger_center = collider.trigger_center or (0, 0, 0)
                has_collider = True
        if has_collider:
            destinfo.setup_events()
            trigger = create_trigger(
                destinfo.event_hit,
                f"DestinationTriggerFor{obj.type}",
                Transform.fill(scale=trigger_radius * 16),
                sphere=True,
            )
            trigger_transform = obj.transform.apply(pos=trigger_center)
            if obj.type == 'VirusSpiritSpawner':
                trigger_transform = trigger_transform.apply(scale=(2.18, 2.18, 2.18))
            else:
                trigger_transform = trigger_transform.apply(pos=(0, 4.8, 0))
            setup_hit_trigger(
                destinfo, trigger,
                trigger_transform)
        try:
            # TurnLightOnNearCar component appears to make teleporters single-use
            del obj['TurnLightOnNearCar']
        except KeyError:
            pass
        else:
            destinfo.lines.append("(single use)")
        self.destinfo_by_subtele[match] = destinfo
        self.destinfos.append(destinfo)
        return destinfo,

    def post_process(self):
        for destinfo in self.destinfos:
            dest_sub_tele = self.filter.tracker.real_dest(destinfo.__sub_teleporter)
            dest_destinfo = self.destinfo_by_subtele.get(dest_sub_tele)
            if dest_destinfo is not None:
                destinfo.dest_destinfos.append(dest_destinfo)


class WarpAnchorMatcher(DestinationMatcher):

    def __init__(self, filter):
        super().__init__(filter)
        self.destinfos = []
        self.destinfo_by_id = {}

    def apply(self, obj, global_transform):
        try:
            frag = obj['WarpAnchor']
        except KeyError:
            return ()
        destinfo = self.filter.create_destination(obj, global_transform)
        destinfo.__warp_anchor = obj
        try:
            tele = next(c for c in obj.children if c.has_any('TeleporterExitCheckpoint'))
            checkpt = tele['TeleporterExitCheckpoint']
        except KeyError:
            pass
        else:
            if checkpt.trigger_checkpoint:
                destinfo.is_checkpoint = True
                checkpt.trigger_checkpoint = 0
        has_collider = False
        if frag.trigger_type == 'sphere':
            try:
                collider = obj['SphereCollider']
            except KeyError:
                pass
            else:
                has_collider = True
                trigger_radius = collider.trigger_radius
                if trigger_radius is None:
                    trigger_radius = 1
                trigger_center = collider.trigger_center or (0, 0, 0)
                trigger_transform = Transform.fill(scale=trigger_radius * .5)
        elif frag.trigger_type == 'box':
            try:
                collider = obj['BoxCollider']
            except KeyError:
                pass
            else:
                has_collider = True
                trigger_size = collider.trigger_size or (1, 1, 1)
                trigger_center = collider.trigger_center or (0, 0, 0)
                trigger_transform = Transform.fill(scale=trigger_size)
        else:
            raise ValueError(f"Unknown trigger_type: {frag.trigger_type}")
        if has_collider:
            destinfo.setup_events()
            trigger = create_trigger(
                destinfo.event_hit,
                f"DestinationTriggerFor{obj.type}",
                trigger_transform,
                sphere=(frag.trigger_type == 'sphere'),
            )
            setup_hit_trigger(
                destinfo, trigger,
                obj.transform.apply(pos=trigger_center),
            )
        self.destinfo_by_id.setdefault(frag.my_id, destinfo)
        self.destinfos.append(destinfo)
        return destinfo,

    def post_process(self):
        for destinfo in self.destinfos:
            frag = destinfo.__warp_anchor['WarpAnchor']
            if frag.is_primary:
                dest_destinfo = self.destinfo_by_id.get(frag.other_id)
                if dest_destinfo is not None:
                    destinfo.dest_destinfos.append(dest_destinfo)


DEST_MATCHER_CLASSES = [v for v in globals().values()
                        if (isinstance(v, type)
                            and issubclass(v, DestinationMatcher)
                            and v is not DestinationMatcher)]


class WarpzoneFilter(ObjectFilter):

    @classmethod
    def add_args(cls, parser):
        super().add_args(parser)
        parser.add_argument(':verbose', action='store_true',
                            help="List destination objects.")

    def __init__(self, args):
        super().__init__(args)
        self.verbose = args.verbose
        self.destinations = []
        self.dest_matchers = [cls(self) for cls in DEST_MATCHER_CLASSES]
        self.position = (10000, 10000, 10000)
        self.startinfos = []
        self.endinfos = []
        self.start_positions = []
        self.end_positions = []
        self.tracker = TeleporterTracker()

    def create_destination(self, obj, global_transform, *, transform=Transform.fill()):
        link_id = self.tracker.new_link_id()
        start_transform = obj.transform.apply(*transform)
        tele = DefaultClasses.level_objects.create(
            'TeleporterExit',
            real_transform=start_transform.set(scale=(1, 1, 1))
        )
        subtele = DefaultClasses.level_subobjects.create('Teleporter')
        subtele.attach_new_fragment('TeleporterExit', version=0, link_id=link_id)
        subtele.attach_new_fragment('TeleporterExitCheckpoint', version=0, trigger_checkpoint=False)
        tele.children = [subtele]
        pos = global_transform.apply(*start_transform).pos
        return DestinationInfo(obj, pos, tele, link_id, [obj.type])

    def downgrade_object(self, obj):
        # Downgrade some fragments so we don't have to bother with
        # checkpoint or teleporter IDs.
        def do_downgrade(obj):
            try:
                frag = obj['TeleporterEntrance']
            except KeyError:
                pass
            else:
                if frag.container.version >= 4:
                    frag.container.version = 3
            try:
                frag = obj['TeleporterExitCheckpoint']
            except KeyError:
                pass
            else:
                if frag.container.version >= 2:
                    frag.container.version = 1
            try:
                frag = obj['CheckpointLogic']
            except KeyError:
                pass
            else:
                if frag.container.version >= 3:
                    frag.container.version = 2
            for child in obj.children:
                do_downgrade(child)
        do_downgrade(obj)

    def filter_object(self, obj, global_transform=Transform.fill(), preparing=False):
        if preparing:
            self.tracker.check_add(obj)
            self.downgrade_object(obj)
            return obj,
        else:
            matches = []
            objects = []
            for matcher in self.dest_matchers:
                matches.extend(matcher.apply(obj, global_transform))
            if not any(1 for m in matches if m.delete_object):
                objects.append(obj)
            if matches:
                self.destinations.extend(matches)
                new_objects = []
                for m in matches:
                    if m.tele is not None:
                        new_objects.append(m.tele)
                    new_objects.extend(o for o in m.add_objects)
                if new_objects:
                    objects.extend(create_replacement_group(
                        obj, new_objects, custom_name=f"WarpzoneDestinationFor{obj.type}"))
            return objects

    def filter_group(self, grp, level, global_transform=Transform.fill(), **kw):
        global_transform = global_transform.apply(*grp.transform)
        return super().filter_group(grp, level,
                                    global_transform=global_transform, **kw)

    def get_sorted_destinations(self):
        def avg_position(positions):
            sx, sy, sz = 0, 0, 0
            for x, y, z in positions:
                sx += x
                sy += y
                sz += z
            n = len(positions)
            return sx / n, sy / n, sz / n

        def distance_between(a, b):
            return ((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2 + (a[2] - b[2]) ** 2) ** .5

        start_position = self.start_positions[0]
        for destinfo in self.destinations:
            avgpos = avg_position(destinfo.positions)
            toend = min(distance_between(p, avgpos) for p in self.end_positions)
            tostart = distance_between(avgpos, start_position)
            destinfo.start_end_fraction = tostart / (toend + tostart)
            destinfo.lines.append(f"~{destinfo.start_end_fraction * 100:.0f}%")
        return sorted(self.destinations, key=attrgetter('start_end_fraction'))

    def create_spline_connection(self, first_transform, source, dest, bidi):
        options = dict(
            mat_color=(.1, .1, .1, 1),
            mat_emit=(1, 1, 1, .5),
            mat_reflect=(0, 0, 0, 0),
            mat_spec=(0, 0, 0, 0),
            image_index=0,
            emit_index=0,
            disable_collision=True,
            additive_transp=True,
            multip_transp=False,
            invert_emit=False,
        )
        if bidi:
            options.update(
                mat_color=(0, .482, 1, 1),
                mat_emit=(0, .482, 1, .5),
                tex_scale=(1, 5, 1),
            )
        cx = (source.entrance_xpos + dest.entrance_xpos) * .5
        spline = DefaultClasses.common.create(
            'GoldenSimple', type='SplineCylinder',
            real_transform=first_transform.apply(pos=(cx, 0, 0), rot=(-2**.5/2, 0, 0, 2**.5/2)),
            **options
        )
        node1 = DefaultClasses.level_subobjects.create(
            'BezierSplineTrackEmpty1',
            real_transform=Transform.fill(pos=(source.entrance_xpos - cx, 0, source.entrance_height))
        )
        node2 = DefaultClasses.level_subobjects.create(
            'BezierSplineTrackEmpty2',
            real_transform=Transform.fill(pos=(dest.entrance_xpos - cx, 0, dest.entrance_height))
        )
        spline.children = [node1, node2]
        if not bidi:
            spline.attach_new_fragment(
                'GoldenAnimator', version=1,
                translate_vector=(0, -10, 0),
                duration=1,
                do_loop=1,
            )
        return spline

    def create_warpzone_objects(self, destinations, p=None):
        objects = []

        base_transform = Transform.fill(pos=self.position)
        cube_size = 64
        tele_space = 32
        tele_distance = 128
        floor_width = 512
        trigger_distance = 512
        marker_reset_trigger_size = 32
        text_ypos = 16
        text_line_height = 5.928
        travel_width = 16
        travel_zpos = 16
        checkpoint_color = (.1, .1, 0, 1)
        spawn_gs_attrs = dict(
            mat_color=(0, 0, 0, 0),
            mat_emit=(.1, .1, 0, 1), # bug: checkpoint overrides this color
            mat_reflect=(0, 0, 0, 0),
            mat_spec=(0, 0, 0, 0),
            image_index=17,
            emit_index=17,
            disable_collision=True,
            additive_transp=False,
            multip_transp=False,
            invert_emit=True,
        )
        floor_attrs = dict(
            mat_color=(1, 1, 1, .4),
            mat_emit=(.38, .38, .38, .12),
            mat_spec=(.03, .03, .03, .2),
            image_index=7,
            emit_index=7,
            tex_scale=(3.125,) * 3,
            tex_offset=(.003, .003, .003),
            disable_reflect=True,
            world_mapped=True,
        )

        floor_length = (len(destinations) + 2) * tele_space
        floor_xpos = ((len(destinations) - 1) % 2) * tele_space * .5
        objects.append(DefaultClasses.level_objects.create(
            'CubeGS',
            transform=base_transform.apply(
                pos=(floor_xpos, 0, tele_distance),
                scale=(floor_length / cube_size, 1, floor_width / cube_size)
            ).apply(pos=(0, cube_size * -.5, 0)),
            **floor_attrs
        ))
        objects.append(DefaultClasses.level_objects.create(
            'CubeGS',
            transform=base_transform.apply(
                pos=(floor_xpos + -floor_length * .5, 0, tele_distance),
                scale=(0.1, 0.1, floor_width / cube_size)
            ).apply(pos=(cube_size * .5, cube_size * .5, 0)),
            **floor_attrs
        ))
        objects.append(DefaultClasses.level_objects.create(
            'CubeGS',
            transform=base_transform.apply(
                pos=(floor_xpos + floor_length * .5, 0, tele_distance),
                scale=(0.1, 0.1, floor_width / cube_size)
            ).apply(pos=(cube_size * -.5, cube_size * .5, 0)),
            **floor_attrs
        ))
        objects.append(DefaultClasses.level_objects.create(
            'EmpirePopUp',
            transform=base_transform.apply(
                pos=(floor_xpos + -floor_length * .5 + tele_space, 0, travel_zpos),
                rot=(0, 2**.5/2, 0, 2**.5/2)
            )
        ))
        objects.append(DefaultClasses.level_objects.create(
            'EmpirePopUp',
            transform=base_transform.apply(
                pos=(floor_xpos + floor_length * .5 - tele_space, 0, travel_zpos),
                rot=(0, -2**.5/2, 0, 2**.5/2)
            )
        ))

        eventname_left = str(random_uuid())
        eventname_right = str(random_uuid())
        eventname_forward_disable = str(random_uuid())
        eventname_forward_enable = str(random_uuid())

        objects.append(DefaultClasses.level_objects.create(
            'InvisibleStartZone',
            transform=base_transform,
        ))

        spawn_objs = []
        # Moving the start zone doesn't change the position, we need a
        # checkpoint for that.
        checkpoint = DefaultClasses.level_objects.create(
            'CheckpointNoVisual',
            transform=Transform.fill(rot=(0, 0, 0, 1))
        )
        checkpoint.attach_new_fragment(
            'CheckpointLogic', version=2,
            start_color=checkpoint_color,
            finish_color=checkpoint_color,
            silent=True,
        )
        spawn_objs.append(checkpoint)
        spawn_objs.append(DefaultClasses.level_objects.create(
            'SphereHDGS',
            transform=Transform.fill(pos=(0, 0, tele_distance), scale=(.3, .03, .3)),
            **spawn_gs_attrs
        ))
        spawn_objs.append(create_trigger(
            eventname_left,
            'GoLeftTrigger',
            Transform.fill(pos=(-tele_space * 17, trigger_distance / 4, tele_distance),
                           scale=(tele_space * 32.8, trigger_distance / 2, trigger_distance)),
        ))
        spawn_objs.append(create_trigger(
            eventname_right,
            'GoRightTrigger',
            Transform.fill(pos=(tele_space * 17, trigger_distance / 4, tele_distance),
                           scale=(tele_space * 32.8, trigger_distance / 2, trigger_distance)),
        ))

        forcezone_left = DefaultClasses.level_objects.create(
            'ForceZoneBox',
            transform=Transform.fill(pos=(-tele_space * .5, 0, travel_zpos), scale=(2, 2, travel_width)),
        )
        forcezone_left.attach_new_fragment(
            'ForceZone', version=0,
            force_direction=(-1, 0, 0),
            global_force=False,
            force_type=ForceType.WIND,
            disable_global_gravity=False,
            wind_speed=3000,
            drag_multiplier=.5,
        )
        forcezone_left.attach_new_fragment(
            'BoxCollider', version=2,
            trigger_size=(1, 1, 1),
        )
        spawn_objs.append(forcezone_left)
        forcezone_right = DefaultClasses.level_objects.create(
            'ForceZoneBox',
            transform=Transform.fill(pos=(tele_space * .5, 0, travel_zpos), scale=(2, 2, travel_width)),
        )
        forcezone_right.attach_new_fragment(
            'ForceZone', version=0,
            force_direction=(1, 0, 0),
            global_force=False,
            force_type=ForceType.WIND,
            disable_global_gravity=False,
            wind_speed=3000,
            drag_multiplier=.5,
        )
        forcezone_right.attach_new_fragment(
            'BoxCollider', version=2,
            trigger_size=(1, 1, 1),
        )
        spawn_objs.append(forcezone_right)

        spawn_objs.append(DefaultClasses.level_objects.create(
            'CubeGS',
            transform=forcezone_left.transform.apply(scale=(.4/64, .15/64, 1/64)),
            **spawn_gs_attrs
        ))
        spawn_objs.append(DefaultClasses.level_objects.create(
            'CubeGS',
            transform=forcezone_right.transform.apply(scale=(.4/64, .15/64, 1/64)),
            **spawn_gs_attrs
        ))

        cooldown_left = DefaultClasses.level_objects.create(
            'CooldownTriggerNoVisual',
            transform=forcezone_left.transform.apply(),
        )
        cooldown_left.attach_new_fragment(
            'BoxCollider', version=2,
            trigger_size=(1, 1, 1),
        )
        spawn_objs.append(cooldown_left)
        cooldown_right = DefaultClasses.level_objects.create(
            'CooldownTriggerNoVisual',
            transform=forcezone_right.transform.apply(),
        )
        cooldown_right.attach_new_fragment(
            'BoxCollider', version=2,
            trigger_size=(1, 1, 1),
        )
        spawn_objs.append(cooldown_right)

        forcezone_forward = DefaultClasses.level_objects.create(
            'ForceZoneBox',
            transform=Transform.fill(pos=(0, .5, 4), scale=(.5, 1, .5)),
        )
        forcezone_forward.attach_new_fragment(
            'ForceZone', version=0,
            force_direction=(0, 0, 1),
            global_force=False,
            force_type=ForceType.WIND,
            disable_global_gravity=False,
            wind_speed=500,
            drag_multiplier=10,
        )
        forcezone_forward.attach_new_fragment(
            'BoxCollider', version=2,
            trigger_size=(1, 1, 1),
        )

        forcezone_fw_vis_left = DefaultClasses.level_objects.create(
            'CubeGS',
            transform=Transform.fill(pos=(-.85, 0, 8), scale=(.01, .005, .1)),
            **spawn_gs_attrs
        )
        forcezone_fw_vis_right = DefaultClasses.level_objects.create(
            'CubeGS',
            transform=Transform.fill(pos=(.85, 0, 8), scale=(.01, .005, .1)),
            **spawn_gs_attrs
        )

        common_forcezone_fw_anim_settings = dict(
            motion_mode='advanced',
            do_rotate=0,
            do_scale=0,
            translate_type='local',
            delay=0,
            do_loop=0,
            do_extend=False,
            curve_type='linear',
            anim_physics=0,
            always_animate=1,
            default_action='none',
            trigger_on_action='play',
            trigger_wait_for_anim_finish=1,
            trigger_off_action='play_reverse',
            trigger_off_wait_for_anim_finish=1,
        )
        forcezone_fw_group_disable = DefaultClasses.level_objects.create(
            'Group',
            children=[forcezone_forward, forcezone_fw_vis_left, forcezone_fw_vis_right],
            custom_name="SpawnBoostOnDemand",
        )
        forcezone_fw_group_disable.attach_new_fragment(
            'Animator', version=11,
            translate_vector=(0, -64, 0),
            duration=.1,
            **common_forcezone_fw_anim_settings
        )
        forcezone_fw_group_disable.attach_new_fragment(
            'EventListener', version=2,
            event_name=eventname_forward_disable,
        )

        forcezone_fw_group = DefaultClasses.level_objects.create(
            'Group',
            transform=Transform.fill(pos=(0, -32, 0)),
            children=[forcezone_fw_group_disable],
            custom_name="SpawnBoostOnDemand",
        )
        forcezone_fw_group.attach_new_fragment(
            'Animator', version=11,
            translate_vector=(0, 32, 0),
            duration=.1,
            **common_forcezone_fw_anim_settings
        )
        forcezone_fw_group.attach_new_fragment(
            'EventListener', version=2,
            event_name=eventname_forward_enable,
        )
        spawn_objs.append(forcezone_fw_group)

        spawn_objs.append(create_trigger(
            eventname_forward_disable,
            'ForwardBoostDisableTrigger',
            Transform.fill(pos=(-1.51, 1, 0), scale=(1, 2, 16)),
        ))
        spawn_objs.append(create_trigger(
            eventname_forward_disable,
            'ForwardBoostDisableTrigger',
            Transform.fill(pos=(1.51, 1, 0), scale=(1, 2, 16)),
        ))
        spawn_objs.append(create_trigger(
            eventname_forward_enable,
            'ForwardBoostEnableTrigger',
            Transform.fill(pos=(0, .5, 0), scale=(1, 1, 8))
        ))

        startgroup_left = DefaultClasses.level_objects.create(
            'Group',
            children=spawn_objs,
            inspect_children=2,
        )
        startgroup_right = DefaultClasses.level_objects.create(
            'Group',
            transform=base_transform,
            children=[startgroup_left],
            custom_name="WarpzoneCheckpoint",
            inspect_children=2,
        )

        common_anim_settings = dict(
            motion_mode='advanced',
            do_rotate=0,
            do_scale=0,
            translate_type='local',
            delay=0,
            duration=.02,
            do_loop=0,
            do_extend=1,
            curve_type='linear',
            anim_physics=0,
            always_animate=1,
            default_action='none',
            trigger_on_action='play',
            trigger_wait_for_anim_finish=1,
            trigger_off_action='stop',
            trigger_off_wait_for_anim_finish=1,
        )

        startgroup_left.attach_new_fragment(
            'Animator', version=11,
            translate_vector=(-tele_space, 0, 0),
            **common_anim_settings,
        )
        startgroup_left.attach_new_fragment(
            'EventListener', version=2,
            event_name=eventname_left,
        )
        startgroup_right.attach_new_fragment(
            'Animator', version=11,
            translate_vector=(tele_space, 0, 0),
            **common_anim_settings,
        )
        startgroup_right.attach_new_fragment(
            'EventListener', version=2,
            event_name=eventname_right,
        )

        objects.append(startgroup_right)

        first_xpos = (len(destinations) - 1) // 2 * -tele_space
        first_transform = base_transform.apply(pos=(first_xpos, 0, tele_distance))
        for i, destinfo in enumerate(destinations):
            xpos = i * tele_space
            obj_transform = first_transform.apply(pos=(xpos, 0, 0))
            if destinfo.link_id is not None:
                # We omit TeleporterExit fragment for our entrance so as to make the
                # game default to a free unconnected link_id.
                subtele = DefaultClasses.level_subobjects.create('Teleporter')
                subtele.attach_new_fragment(
                    'TeleporterEntrance',
                    version=3,
                    destination=destinfo.link_id,
                    include_in_track_calculation=(i == 0),
                )
                obj = DefaultClasses.level_objects.create(
                    'Teleporter',
                    children=[subtele],
                    transform=obj_transform,
                )
            else:
                obj = DefaultClasses.level_objects.create(
                    'VirusNode01',
                    real_transform=obj_transform.apply(scale=(2, 2, 2)),
                )
            lines = [str(i)] + destinfo.lines
            text_height = text_line_height * len(lines)
            world_text = DefaultClasses.level_objects.create(
                'WorldText',
                transform=obj_transform.apply(pos=(0, text_ypos + text_height * .5, 0), scale=(.75, .75, .75)),
                text='\n'.join(lines),
            )
            world_text.attach_new_fragment(
                'LookAtPlayer', version=3,
                rotation_speed=100,
                target_type="camera",
            )
            if destinfo.is_checkpoint:
                objects.append(DefaultClasses.level_objects.create(
                    'SphereHDGS',
                    transform=obj_transform.apply(pos=(0, 10, 0), scale=(.22, .02, .22)),
                    mat_color=(0, 0, 0, 0),
                    mat_emit=(.115, .588, .331, .2),
                    mat_reflect=(0, 0, 0, 0),
                    mat_spec=(0, 0, 0, 0),
                    disable_collision=True,
                    additive_transp=True,
                    multip_transp=False,
                    invert_emit=True,
                ))
            objects.append(obj)
            objects.append(world_text)
            if destinfo.has_hit_events:
                reset_trigger = create_trigger(
                    destinfo.event_reset,
                    "DestinationResetTrigger",
                    Transform.fill(
                        pos=(0, marker_reset_trigger_size / 4, 0),
                        scale=(floor_length, marker_reset_trigger_size / 2, marker_reset_trigger_size)),
                )
                hit_marker = DefaultClasses.level_objects.create(
                    'SphereHDGS',
                    transform=Transform.fill(
                        pos=(xpos - floor_xpos + first_xpos, 4.8, 0),
                        rot=(2**.5/2, 0, 0, 2**.5/2),
                        scale=(.3, .02, .3)),
                    mat_color=(.5, .2, 0, 1),
                    mat_emit=(.5, .2, 0, .2),
                    mat_reflect=(0, 0, 0, 0),
                    mat_spec=(0, 0, 0, 0),
                    disable_collision=True,
                    additive_transp=False,
                    multip_transp=False,
                    invert_emit=True,
                )
                reset_group_inner = DefaultClasses.level_objects.create(
                    'Group',
                    children=[reset_trigger, hit_marker],
                    custom_name="DestinationResetGroupInner",
                )
                reset_group_inner.attach_new_fragment(
                    'Animator', version=11,
                    translate_vector=(0, marker_reset_trigger_size, 0),
                    translate_type='global_',
                    **common_destination_anim_settings
                )
                reset_group_inner.attach_new_fragment(
                    'EventListener', version=2,
                    event_name=destinfo.event_hit,
                )
                reset_group = DefaultClasses.level_objects.create(
                    'Group',
                    transform=base_transform.apply(pos=(floor_xpos, -marker_reset_trigger_size, tele_distance)),
                    children=[reset_group_inner],
                    custom_name="DestinationResetGroup",
                )
                reset_group.attach_new_fragment(
                    'Animator', version=11,
                    translate_vector=(0, -marker_reset_trigger_size, 0),
                    translate_type='local',
                    **common_destination_anim_settings
                )
                reset_group.attach_new_fragment(
                    'EventListener', version=2,
                    event_name=destinfo.event_reset,
                )
                objects.append(reset_group)
            destinfo.entrance_xpos = xpos
            destinfo.entrance_height = text_ypos + text_height

        bidi_created = set()
        for destinfo in destinations:
            for dest_destinfo in destinfo.dest_destinfos:
                bidi = destinfo in dest_destinfo.dest_destinfos
                if bidi:
                    if (dest_destinfo, destinfo) in bidi_created:
                        continue
                    bidi_created.add((destinfo, dest_destinfo))
                objects.append(self.create_spline_connection(first_transform, destinfo, dest_destinfo, bidi))

        return objects

    def apply(self, content, p=None):
        if not isinstance(content, Level):
            raise ValueError("warpzone filter can only "
                             "be used with Levels.")
        if not super().apply(content, preparing=True):
            return False
        self.tracker.calculate()
        self.tracker.normalize()
        if not super().apply(content, preparing=False):
            return False
        for matcher in self.dest_matchers:
            matcher.post_process()

        destinations = self.destinations
        if self.start_positions and self.end_positions:
            try:
                destinations = self.get_sorted_destinations()
            except Exception as e:
                if p:
                    p(f"Destination sorting failed")
                    p.print_exception(e)

        objects = self.create_warpzone_objects(destinations, p)
        content.add_layer("Warpzone", objects=objects)

        if p:
            p(f"Destinations: {len(destinations)}")
            if self.verbose:
                with p.tree_children(len(destinations)):
                    for destinfo in destinations:
                        p.tree_next_child()
                        objs = [o.type for o in destinfo.objects]
                        p(f"Object: {', '.join(objs)}")

        return True


# vim:set sw=4 ts=8 sts=4 et:
