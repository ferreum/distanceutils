"""Filter for removing fog."""


from distance.filter.base import ObjectFilter


class UnfogFilter(ObjectFilter):

    def __init__(self, args):
        super().__init__(args)
        self.num_matched = 0
        self.num_changed = 0
        self.errors = []

    def filter_object(self, obj):
        try:
            frag = obj['Biodome']
        except KeyError as e:
            if e.is_present:
                self.errors.append((obj, obj.get_any('Biodome').container.version))
        else:
            changed = 0
            if frag.sky_far is not None and frag.sky_far < 1500:
                frag.sky_far = 1500.0
                changed = 1
            if frag.fog_enabled:
                frag.fog_enabled = 0
                changed = 1
            self.num_matched += 1
            if changed:
                self.num_changed += 1
        return obj,

    def apply_level(self, content):
        settings = content.settings

        changed = 0
        if settings.sky_far is not None and settings.sky_far < 1500:
            changed = 1
            settings.sky_far = 1500.0
        if settings.fog_enabled:
            changed = 1
            settings.fog_enabled = 0
        self.num_matched += 1
        if changed:
            self.num_changed += 1

        super().apply_level(content)

    def print_summary(self, p):
        if self.num_matched:
            p(f"Objects matched: {self.num_matched}")
            p(f"Objects changed: {self.num_changed}")
        if self.errors:
            p(f"Errors: {len(self.errors)}")
            for target, version in self.errors:
                with p.tree_children():
                    p(f"{target.type!r}: Unknown BiodomeFragment version: {version}")

# vim:set sw=4 ts=8 sts=4 et:
