"""Base classes for filters."""


from distance import Level
from distance.bytes import Section
from distance.base import Transform
from distance.classes import DefaultClasses
from collections import defaultdict


_ANIM_FRAG_TAGS = (
    'Animator',
    'EventListener',
    'TrackAttachment',
)

ANIM_FRAG_SECTIONS = {DefaultClasses.fragments.get_base_key(tag)
                      for tag in _ANIM_FRAG_TAGS}


Group = DefaultClasses.common.klass('Group')


def create_replacement_group(orig, objs, *, animated_only=False, custom_name=None):
    copied_frags = []
    for i, sec in enumerate(orig.sections):
        if sec.to_key(noversion=True) in ANIM_FRAG_SECTIONS:
            copyfrag = orig.fragments[i].clone()
            copyfrag.container = Section(sec, id=None)
            copied_frags.append(copyfrag)
    if animated_only and not copied_frags:
        return objs
    group = Group(children=objs, custom_name=custom_name)
    group.retransform(orig.transform)
    group.fragments += copied_frags
    return group,


class ObjectFilter(object):

    @classmethod
    def add_args(cls, parser):
        parser.add_argument(":maxrecurse", type=int, default=-1,
                            help="Set recursion limit, -1 for infinite (the default).")

    def __init__(self, args):
        self.maxrecurse = args.maxrecurse

    def filter_object(self, obj):
        return obj,

    def filter_group(self, grp, levels, **kw):
        orig_empty = not grp.children
        grp.children = self.filter_objects(grp.children, levels, **kw)
        if not orig_empty and not grp.children:
            # remove empty group
            return ()
        return grp,

    def filter_any_object(self, obj, levels, **kw):
        if obj.is_object_group:
            if levels == 0:
                return obj,
            return self.filter_group(obj, levels - 1, **kw)
        else:
            return self.filter_object(obj, **kw)

    def filter_objects(self, objects, levels, **kw):
        res = []
        for obj in objects:
            res.extend(self.filter_any_object(obj, levels, **kw))
        return res

    def apply_level(self, level, **kw):
        for layer in level.layers:
            layer.objects = self.filter_objects(layer.objects, self.maxrecurse, **kw)

    def apply_group(self, grp, **kw):
        # not using filter_group, because we never want to remove the root
        # group object
        grp.children = self.filter_objects(grp.children, self.maxrecurse, **kw)

    def apply(self, content, p=None, **kw):
        if isinstance(content, Level):
            self.apply_level(content, **kw)
        elif isinstance(content, Group):
            self.apply_group(content, **kw)
        else:
            raise TypeError(f'Unknown object type: {type(content).__name__!r}')
        if not self.post_filter(content):
            return False
        if p:
            self.print_summary(p)
        return True

    def post_filter(self, content):
        return True

    def print_summary(self, p):
        pass


class DoNotApply(Exception):

    def __init__(self, reason=None, *args, **kw):
        super().__init__(*args, **kw)
        self.reason = reason


class ObjectMapper(object):

    def __init__(self, pos=(0, 0, 0), rot=(0, 0, 0, 1), scale=(1, 1, 1)):
        self.transform = Transform.fill(pos=pos, rot=rot, scale=scale)

    def _apply_transform(self, transform, global_transform=Transform.fill()):
        if not transform.verify_rotation(self.transform.rot):
            raise DoNotApply('locked_scale')
        res = transform.apply(*self.transform)
        # Skip replacement if our rotation is not possible inside a
        # non-uniformly scaled group.
        if not global_transform.verify_rotation(res.rot):
            raise DoNotApply('locked_scale_group')
        return res

    def apply(self, obj, global_transform=Transform.fill(), **kw):
        transform = self._apply_transform(obj.transform,
                                          global_transform=global_transform)
        return self.create_result(obj, transform, **kw)

    def create_result(self, old, transform):
        raise NotImplementedError


class TeleporterTracker(object):

    def __init__(self):
        self.teleporters = []

    def add(self, obj):
        self.teleporters.append(obj)

    def check_add(self, obj):
        self.teleporters.extend(
            c for c in obj.children
            if c.has_any('TeleporterEntrance') or c.has_any('TeleporterExit'))

    def calculate(self):
        entrances = defaultdict(list)
        exits = defaultdict(list)
        tele_destinations = {}
        tele_link_ids = {}
        for tele in self.teleporters:
            dest, link_id = None, None
            try:
                dest = tele['TeleporterEntrance'].destination
            except KeyError:
                pass
            try:
                link_id = tele['TeleporterExit'].link_id
            except KeyError:
                pass
            if link_id is not None:
                exits[link_id].append(tele)
            if dest is not None:
                entrances[dest].append(tele)
            tele_destinations[tele] = dest
            tele_link_ids[tele] = link_id
        self.entrances = dict(entrances)
        self.exits = dict(exits)
        self.tele_destinations = tele_destinations
        self.tele_link_ids = tele_link_ids
        self.next_link_id = None

    def real_dest(self, obj, *, include_self=False):
        if obj is None:
            return None
        destination = self.tele_destinations.get(obj)
        dests = self.exits.get(destination, ())
        for dest in dests:
            if dest is not obj:
                return dest
        if include_self and dests:
            return obj
        # not connected, or teleports to self and include_self is false
        return None

    def iter_entrances(self, obj, *, include_self=False):
        link_id = self.tele_link_ids[obj]
        entrances = self.entrances.get(link_id, ())
        for e in entrances:
            if self.real_dest(e, include_self=include_self) is obj:
                yield e

    def normalize(self):
        cur_id = 4
        for tele in self.teleporters:
            if (tele.has_any('TeleporterEntrance')
                    and self.real_dest(tele, include_self=True) is None):
                # teleporter leads nowhere; assign non-existing destination ID
                tele['TeleporterEntrance'].destination = cur_id
                cur_id += 1
            if tele.has_any('TeleporterExit'):
                tele['TeleporterExit'].link_id = cur_id
                for entrance in self.iter_entrances(tele, include_self=True):
                    entrance['TeleporterEntrance'].destination = cur_id
                cur_id += 1
        self.calculate()

    def new_link_id(self):
        link_id = self.next_link_id
        if link_id is None:
            link_id = 4
        while link_id in self.exits or link_id in self.entrances:
            link_id += 1
        self.next_link_id = link_id + 1
        return link_id


# vim:set sw=4 ts=8 sts=4 et:
